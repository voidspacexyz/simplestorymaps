import config from './config.js';

function setPageTitle(title) {
    document.title = title + " powered by Simple Story Maps";
    document.getElementById('pageTitle').innerText = title
}
setPageTitle(config.title);

  
function showDescriptionModal() {
const modal = document.getElementById('descriptionModal');
modal.style.zIndex = '1000'; // Add a higher z-index value
modal.classList.remove('hidden');
document.getElementById('modal-title').innerText = config.title
document.getElementById('modal-description').innerHTML = config.description
}

function closeDescriptionModal() {
const modal = document.getElementById('descriptionModal');
modal.classList.add('hidden');
}

// Add event listener to the About button
const aboutButton = document.getElementById('about-button');
aboutButton.addEventListener('click', showDescriptionModal);
const closeButton = document.getElementById('close-button');
closeButton.addEventListener('click',closeDescriptionModal);