# Simple Map Stories

The tool takes a point-types geojson and plots it on a map. It also supports Audio/Video to play and also refer a link. You can have categories and filter by the same.

The tool currently can do the following

1. Take a list of categories and assign a colour to it
2. The a list of points as a geojson, will attributes, and create a point in the selected cateory colour and display the metadata attributes

Note: [Gitlab repo](https://gitlab.com/voidspacexyz/simplestorymaps) is a readonly mirror. The primary codebase is on [Codeberg](https://codeberg.org/voidspacexyz/simplestorymaps). 


# How to setup this tool

1. Clone this repository into your root directory for the static site.
2. copy js/config.js.sample to js/config.js
3. Refer to sample/ on how to write the respective json files
4. Create a directory called data/ and store your json files inside the same
5. Assuming your site is mapstories.example.com and your json files are placed in data/ directory, then your categories url will be mapstories.example.com/data/categories.json and data url will be mapstories.example.com/data/data.geojson
6. Write your nginx or apache or any webserver of your choice to render a static site. Below is an nginx sample for rendering the site as mapstories.example.com

    ```
    server {
        server_name mapstories.example.com;
        listen 80;
        root /path/to/project;
        index index.html;
        access_log /path/to/access.log;
        error_log /path/to/error.log;
    }
    ```
7. Run certbot for automatic ssl and your site is live.


Note: This tool is intentionally not built to use cool frameworks, and it will not be re-written in frameworks like React or Vue at any point in time.


